{
  "abstract" : {
    "pref" : "Ziel des Projektes ist es, Daten zur Verfügung zu stellen, die erforderlich sind, um die deskriptive Vertretung der Bürger mit Migrationshintergrund (CIOs) zu erforschen. Das Hauptziel ist es, einen Überblick über das soziale und politische Profil von Abgeordneten zu geben, mit besonderem Fokus auf der Identifizierung von Abgeordneten mit Migrationshintergrund. Zusätzlich zu dem unten beschriebenen Datensatz auf nationaler Ebene steht ein entsprechender regionaler Datensatz zur Verfügung. \nIdentifikationsvariablen: Politische Ebene (regional, national); Land-ID (NUTS); Name der Region; Region-ID (NUTS); Datum der entsprechenden Wahl; vollständiger Name des Wahlbezirks, in dem gewählt wurde; Niveau der Wahlstufe (Bezirk / Wahlkreis); Kennung für Wahlbezirke 1 bis 3 auf nationaler Ebene; Anzahl der Legislaturperioden im Land, wie vom Parlament selbst aufgezeichnet; Datum von Beginn und Ende der Legislaturperiode; Vorname, erster (zweiter) Nachname des MP; MP-ID; nationaler Abgeordneter ist gleichzeitig auch regionaler Abgeordneter; welcher regionale Abgeordnete. \nSozio-demographische Variablen: Geschlecht des MP; Geburtsjahr des MP; höchstes Bildungsniveau (ISCED 1997); letzter Beruf / Beruf des Abgeordneten, bevor er jemals Abgeordneter wurde (ISCO 2008); Branche des Berufs zum Zeitpunkt der ersten Wahl; derzeitige Berufstätigkeit / Beruf des Abgeordneten (ISCO 2008); Branche des derzeitigen Berufs. \nVariablen mit Bezug auf die Wahl und parlamentarische Amtszeit: Häufigkeit der Wahl des Abgeordneten in das Parlament zuvor in diesem Bezirk; Art des Wahlbezirks; Anzahl der Zeiten, in denen der Abgeordnete zuvor in diesem Wahlbezirk zum Parlament gewählt wurde; Anfänger; Häufigkeit der Wahl des Abgeordneten zum Parlament; Häufigkeit der Annahme des Sitzes im Parlament nachdem der Abgeordnete gewählt wurde; Jahr, in dem der Abgeordnete zum ersten Mal in das nationale / regionale Parlament gewählt wurde; Gesamtzahl der Jahre als Abgeordneter im nationalen / regionalen Parlament vor dieser Legislaturperiode; Zeitpunkt, wann der Abgeordnete zum letzten Mal vor dieser Legislaturperiode gewählt wurde (Kontinuität); MP wurde mit dem Amtsantritt in die Kammer gewählt; keine Unterbrechungen zwischen der Aufnahme des Sitzes bis zum Ende der Legislaturperiode; Anzahl der Monate, die der Abgeordnete im Amt war (falls keine Legislaturperiode); Rückkehr des MP, um einen verlorenen Sitz zurückzuerobern; Position auf der Parteiliste; Rangposition, in der der Abgeordnete im Bezirk gewählt wurde; doppelte Kandidatur; MP gewann den Sitz als Amtsinhaber oder als Kandidat; parlamentarische Gruppe der Partei zu Beginn und am Ende der Amtszeit; vollständiger Name und Akronym der Partei oder Liste; Partei-Code nach CMP- Datensatz (Comparative Manifesto Project); Partei-ID. \nVariablen zur Herkunft als Einwanderer (entsprechende Kodierung für Mutter und Vater des Abgeordneten): MP ist im Land des Parlaments geboren; Land (ISO 3166-1), Weltregion (UN-Klassifikation für ´Zusammensetzung der makrogeographischen Regionen´) und Länderregion (NUTS), in der der Abgeordnete geboren wurde; Datenquellen für Geburtsland (z. B. offizielle parlamentarische Quelle, persönliche Blogs usw.); spezifische Quellen für Geburtsland; Zuverlässigkeit der Daten über das Geburtsland des Abgeordneten (wie vom Codierer beurteilt); Jahr der Einwanderung; geboren als Staatsbürger des Landes des Parlaments; Land der Nationalität bei der Geburt; Datenquellen für das Land der Staatsangehörigkeit bei der Geburt; spezifische Quellen für das Land der Staatsbürgerschaft bei der Geburt; Zuverlässigkeit der Daten über die Staatsbürgerschaft bei der Geburt; Jahr der Einbürgerung; Datenquellen für das Jahr der Einbürgerung; spezifische Quellen für das Datum der Einbürgerung; Zuverlässigkeit der Daten zur Einbürgerung. \nVariablen im Hinblick auf Aspekte im Zusammenhang mit möglicher Diskriminierung: MP ist Muttersprachler einer offiziellen Landessprache und Datenquellen; spezifische Quellen für die Muttersprache des MP; MP kann von Wählern als Mitglied einer ´identifizierbaren´ Minderheit wahrgenommen werden; Quelle für das gefundene Foto; spezifische Quellen für das Foto des MP; Selbstidentifikation des MP als Mitglied einer ethnischen Minderheit; Ethnizität; Quellen und spezifische Quellen für Informationen über die ethnische Selbstidentifikation des MP; Selbstidentifikation als Mitglied einer bestimmten Religion; Religion, mit der MP sich identifiziert. \nVariablen zur Parteikarriere und Mitgliedschaft in Ausschüssen: Jahr des Beitritts in die Partei, für die der Abgeordnete in dieser Legislaturperiode gewählt wurde; höchste Position innerhalb der Partei; MP wechselte die Parteizugehörigkeit während der Legislaturperiode; Datum des Wechsels; vollständiger Name und Akronym der neuen Partei, CMP-Code der neuen Partei und Pathways-ID für diese Partei; (entsprechende Kodierung, wenn MP die Parteizugehörigkeit während der Legislaturperiode ein zweites Mal wechselte); jemals Gemeinderat oder Bürgermeister vor oder während der Wahl zum Abgeordneten in dieser Legislaturperiode; Gesamtzahl der Jahre im Gemeinderat als Bürgermeister und / oder Ratsmitglied; jemals Mitglied des regionalen Parlaments vor der Wahl zum nationalen Abgeordneten in dieser Legislaturperiode; Anzahl der Jahre im Regionalparlament vor dieser Legislaturperiode; jemals ein Mitglied des Europäischen Parlaments vor der Wahl zum regionalen / nationalen Abgeordneten in dieser Legislaturperiode; Anzahl der Jahre als Mitglied des Europäischen Parlaments; Amt als Minister des nationalen Kabinetts während dieser Legislaturperiode; Anzahl der Monate im Amt als Minister des nationalen Kabinetts; gleichzeitiges Amt als Minister des nationalen Kabinetts / Ratsherr; Anzahl der Monate; MP hatte zuvor ein Amt als Minister des nationalen bzw. des regionalen Kabinetts; Anzahl der Monate; vorherige Parteimitgliedschaft; vollständiger Name und Akronym der vorherigen Partei, Partei-Code nach dem CMP-Dataset, Pathways-ID für die vorherige Partei; Anzahl der Jahre der formellen Zugehörigkeit zur vorherigen Partei; Jahr des ersten Parteibeitritts; Mitgliedschaft in Ausschüssen; Gesamtzahl der parlamentarischen Ausschüsse, in denen die Abgeordneten während dieser Legislaturperiode Mitglied war; Name des Ausschusses, in dem der MP am längsten tätig war; Rolle oder Position im Ausschuss 1 und 2; Gesamtzahl der Monate als Mitglied des Ausschusses während dieser Legislaturperiode; Mitglied dieses Ausschusses während einer früheren Legislaturperiode; Art des Ausschusses; Relevanz des Ausschusses; Fokussierung des Ausschusses auf das Thema Einwanderung; Namen aller anderen Ausschüsse, den der Abgeordnete während der Legislaturperiode beigetreten ist; Mitgliedschaft in einer All-Parteien-Gruppe (APG), Freundschaftsgruppe oder Themengruppe, die in der persönlichen (parlamentarischen) Website erwähnt wird; Tätigkeit des MP während dieser Amtszeit als Vorsitzender, Sprecher, Präsident der Kammer, als stellvertretender Vorsitzender, stellvertretender Sprecher, stellvertretender Präsident der Kammer, als Mitglied des politischen Verwaltungs- / Managementteams der Kammer, als Mitglied der parlamentarischen Führungsgruppe der Partei. \nZusätzlich verkodet wurde: andere Beobachtungen; MP bzw. dessen Mutter und Vater sind Ausgewiesene; Name und Nachname Griechisch; Titular MP; Regierungsmitglied und Zeit in der Regierung.",
    "de" : "Ziel des Projektes ist es, Daten zur Verfügung zu stellen, die erforderlich sind, um die deskriptive Vertretung der Bürger mit Migrationshintergrund (CIOs) zu erforschen. Das Hauptziel ist es, einen Überblick über das soziale und politische Profil von Abgeordneten zu geben, mit besonderem Fokus auf der Identifizierung von Abgeordneten mit Migrationshintergrund. Zusätzlich zu dem unten beschriebenen Datensatz auf nationaler Ebene steht ein entsprechender regionaler Datensatz zur Verfügung. \nIdentifikationsvariablen: Politische Ebene (regional, national); Land-ID (NUTS); Name der Region; Region-ID (NUTS); Datum der entsprechenden Wahl; vollständiger Name des Wahlbezirks, in dem gewählt wurde; Niveau der Wahlstufe (Bezirk / Wahlkreis); Kennung für Wahlbezirke 1 bis 3 auf nationaler Ebene; Anzahl der Legislaturperioden im Land, wie vom Parlament selbst aufgezeichnet; Datum von Beginn und Ende der Legislaturperiode; Vorname, erster (zweiter) Nachname des MP; MP-ID; nationaler Abgeordneter ist gleichzeitig auch regionaler Abgeordneter; welcher regionale Abgeordnete. \nSozio-demographische Variablen: Geschlecht des MP; Geburtsjahr des MP; höchstes Bildungsniveau (ISCED 1997); letzter Beruf / Beruf des Abgeordneten, bevor er jemals Abgeordneter wurde (ISCO 2008); Branche des Berufs zum Zeitpunkt der ersten Wahl; derzeitige Berufstätigkeit / Beruf des Abgeordneten (ISCO 2008); Branche des derzeitigen Berufs. \nVariablen mit Bezug auf die Wahl und parlamentarische Amtszeit: Häufigkeit der Wahl des Abgeordneten in das Parlament zuvor in diesem Bezirk; Art des Wahlbezirks; Anzahl der Zeiten, in denen der Abgeordnete zuvor in diesem Wahlbezirk zum Parlament gewählt wurde; Anfänger; Häufigkeit der Wahl des Abgeordneten zum Parlament; Häufigkeit der Annahme des Sitzes im Parlament nachdem der Abgeordnete gewählt wurde; Jahr, in dem der Abgeordnete zum ersten Mal in das nationale / regionale Parlament gewählt wurde; Gesamtzahl der Jahre als Abgeordneter im nationalen / regionalen Parlament vor dieser Legislaturperiode; Zeitpunkt, wann der Abgeordnete zum letzten Mal vor dieser Legislaturperiode gewählt wurde (Kontinuität); MP wurde mit dem Amtsantritt in die Kammer gewählt; keine Unterbrechungen zwischen der Aufnahme des Sitzes bis zum Ende der Legislaturperiode; Anzahl der Monate, die der Abgeordnete im Amt war (falls keine Legislaturperiode); Rückkehr des MP, um einen verlorenen Sitz zurückzuerobern; Position auf der Parteiliste; Rangposition, in der der Abgeordnete im Bezirk gewählt wurde; doppelte Kandidatur; MP gewann den Sitz als Amtsinhaber oder als Kandidat; parlamentarische Gruppe der Partei zu Beginn und am Ende der Amtszeit; vollständiger Name und Akronym der Partei oder Liste; Partei-Code nach CMP- Datensatz (Comparative Manifesto Project); Partei-ID. \nVariablen zur Herkunft als Einwanderer (entsprechende Kodierung für Mutter und Vater des Abgeordneten): MP ist im Land des Parlaments geboren; Land (ISO 3166-1), Weltregion (UN-Klassifikation für ´Zusammensetzung der makrogeographischen Regionen´) und Länderregion (NUTS), in der der Abgeordnete geboren wurde; Datenquellen für Geburtsland (z. B. offizielle parlamentarische Quelle, persönliche Blogs usw.); spezifische Quellen für Geburtsland; Zuverlässigkeit der Daten über das Geburtsland des Abgeordneten (wie vom Codierer beurteilt); Jahr der Einwanderung; geboren als Staatsbürger des Landes des Parlaments; Land der Nationalität bei der Geburt; Datenquellen für das Land der Staatsangehörigkeit bei der Geburt; spezifische Quellen für das Land der Staatsbürgerschaft bei der Geburt; Zuverlässigkeit der Daten über die Staatsbürgerschaft bei der Geburt; Jahr der Einbürgerung; Datenquellen für das Jahr der Einbürgerung; spezifische Quellen für das Datum der Einbürgerung; Zuverlässigkeit der Daten zur Einbürgerung. \nVariablen im Hinblick auf Aspekte im Zusammenhang mit möglicher Diskriminierung: MP ist Muttersprachler einer offiziellen Landessprache und Datenquellen; spezifische Quellen für die Muttersprache des MP; MP kann von Wählern als Mitglied einer ´identifizierbaren´ Minderheit wahrgenommen werden; Quelle für das gefundene Foto; spezifische Quellen für das Foto des MP; Selbstidentifikation des MP als Mitglied einer ethnischen Minderheit; Ethnizität; Quellen und spezifische Quellen für Informationen über die ethnische Selbstidentifikation des MP; Selbstidentifikation als Mitglied einer bestimmten Religion; Religion, mit der MP sich identifiziert. \nVariablen zur Parteikarriere und Mitgliedschaft in Ausschüssen: Jahr des Beitritts in die Partei, für die der Abgeordnete in dieser Legislaturperiode gewählt wurde; höchste Position innerhalb der Partei; MP wechselte die Parteizugehörigkeit während der Legislaturperiode; Datum des Wechsels; vollständiger Name und Akronym der neuen Partei, CMP-Code der neuen Partei und Pathways-ID für diese Partei; (entsprechende Kodierung, wenn MP die Parteizugehörigkeit während der Legislaturperiode ein zweites Mal wechselte); jemals Gemeinderat oder Bürgermeister vor oder während der Wahl zum Abgeordneten in dieser Legislaturperiode; Gesamtzahl der Jahre im Gemeinderat als Bürgermeister und / oder Ratsmitglied; jemals Mitglied des regionalen Parlaments vor der Wahl zum nationalen Abgeordneten in dieser Legislaturperiode; Anzahl der Jahre im Regionalparlament vor dieser Legislaturperiode; jemals ein Mitglied des Europäischen Parlaments vor der Wahl zum regionalen / nationalen Abgeordneten in dieser Legislaturperiode; Anzahl der Jahre als Mitglied des Europäischen Parlaments; Amt als Minister des nationalen Kabinetts während dieser Legislaturperiode; Anzahl der Monate im Amt als Minister des nationalen Kabinetts; gleichzeitiges Amt als Minister des nationalen Kabinetts / Ratsherr; Anzahl der Monate; MP hatte zuvor ein Amt als Minister des nationalen bzw. des regionalen Kabinetts; Anzahl der Monate; vorherige Parteimitgliedschaft; vollständiger Name und Akronym der vorherigen Partei, Partei-Code nach dem CMP-Dataset, Pathways-ID für die vorherige Partei; Anzahl der Jahre der formellen Zugehörigkeit zur vorherigen Partei; Jahr des ersten Parteibeitritts; Mitgliedschaft in Ausschüssen; Gesamtzahl der parlamentarischen Ausschüsse, in denen die Abgeordneten während dieser Legislaturperiode Mitglied war; Name des Ausschusses, in dem der MP am längsten tätig war; Rolle oder Position im Ausschuss 1 und 2; Gesamtzahl der Monate als Mitglied des Ausschusses während dieser Legislaturperiode; Mitglied dieses Ausschusses während einer früheren Legislaturperiode; Art des Ausschusses; Relevanz des Ausschusses; Fokussierung des Ausschusses auf das Thema Einwanderung; Namen aller anderen Ausschüsse, den der Abgeordnete während der Legislaturperiode beigetreten ist; Mitgliedschaft in einer All-Parteien-Gruppe (APG), Freundschaftsgruppe oder Themengruppe, die in der persönlichen (parlamentarischen) Website erwähnt wird; Tätigkeit des MP während dieser Amtszeit als Vorsitzender, Sprecher, Präsident der Kammer, als stellvertretender Vorsitzender, stellvertretender Sprecher, stellvertretender Präsident der Kammer, als Mitglied des politischen Verwaltungs- / Managementteams der Kammer, als Mitglied der parlamentarischen Führungsgruppe der Partei. \nZusätzlich verkodet wurde: andere Beobachtungen; MP bzw. dessen Mutter und Vater sind Ausgewiesene; Name und Nachname Griechisch; Titular MP; Regierungsmitglied und Zeit in der Regierung.",
    "en" : "The project aims at providing the data required to study the descriptive representation of citizens of immigrant origin (CIOs). The main aim is to provide an overview of the social and political profile of Member of Parliament (MPs), with a particular focus on identifying MPs of immigrant origin. In addition to the national level dataset described below, a corresponding regional level dataset is available.\nIdentification variables: Political level (regional, national); country-ID (NUTS); name of region; region-id (NUTS); date of relevant election; full name of district in which elected; level of electoral tier (first / Lower (or single tier); identifier for tier 1 to 3 districts at national level; number of legislatures in the country, as recorded by the parliament itself; date in which the legislature begins and ends; first name, first (second) surname of MP; MP-ID;  national MP is also simultaneously a regional MP; which regional MP. Demography: sex of MP; year of birth of MP; highest level of education (ISCED 1997); last occupation /profession of the MP before first ever becoming an MP (ISCO 2008); occupation sector when first elected; current occupation/ profession of the MP (ISCO 2008); current occupation sector.\nElectoral and parliamentary tenure variables:  number of times the MP has been previously elected to parliament in this district; type of electoral district; number of times the MP has been previously elected to parliament in this tier; Rookie: MP elected for the first time in this term; number of times the MP has been elected to parliament; number of times the MP has taken up the seat in parliament once elected; year when the MP was first elected to national/regional parliament; total number of years spent in national/regional parliament as MP, prior to this legislature (seniority);  when was the MP elected for the last time prior to this legislature (continuity); MP was elected to chamber from inauguration; MP stayed continuously with no interruptions from the moment of taking up the seat until the end of the legislative term; number of months the MP did serve (if he did not serve a full legislative term); MP came back to reclaim the seat if MP left seat at some point; position in party list; rank position in which the MP was elected in district; double candidacy in another tier; MP won seat as incumbent, or as contender; parliamentary group the MP joined at the beginning and at the end of his/her term; full name and acronym of party or list in which elected; party code according to the CMP (Comparative Manifesto Project) dataset; party-ID.\nImmigrant origin variables (corresponding coding for MPs mother and father): MP was born in the country of parliament; country (ISO 3166-1), world region (UN Classification for ‘Composition of macro geographical regions’), and country region (NUTS) in which the MP was born; data sources for country of birth (e.g. official parliamentary source, personal blogs, etc.); specific sources for  country of birth; reliability of the data regarding the country of birth of the MP (as judged by the coder); year of immigration; born as a national citizen of the country of parliament; country of nationality at birth; data sources country of nationality at birth; specific sources for country of citizenship at birth; reliability of the data regarding citizenship at birth; year in which naturalized as a citizen; data sources year of naturalization; specific sources for date of naturalization; reliability of the data regarding naturalization.\nVariables relating to aspects potentially related to discrimination: the MP is a native speaker of an official country language and data sources; specific sources for native language of MP; MP can be perceived by voters as a member of an ‘identifiable’ minority; source where picture found; specific sources for picture of MP; does the MP self-identify as a member of an ethnic minority; ethnicity; sources and specific sources for information on ethnic self-identification of MP; self-identification as a member of a certain religion; religion the MP identifies with.\nParty career and committee membership variables: year in which the MP joined the party for which she/he was elected in this legislative term; highest position within the party; MP changed party affiliation during the legislative term; date of change; full name and party acronym of the new party joined, CMP code of the new party and Pathways identifier for party; (corresponding coding if the MP changed party affiliation during the legislative term a second time); ever a local councilor or mayor prior to, or while,  being elected an MP this legislative term; total number of years in the local council as a mayor and/or councilor; ever a member of the regional parliament prior to being elected a national MP this legislative term; number of years in the regional parliament prior to this legislature; ever a member of the European Parliament prior to being elected a regional/national MP this legislative term; number of years as a member of the European Parliament; MP served as a national cabinet minister during this legislative term; number of months the MP served as national cabinet minister; MP simultaneously served as a regional cabinet minister/councilor; number of months;  MP has previously served as a national cabinet minister; number of months; MP has previously served as a regional cabinet minister; number of months; previous party membership; full name and acronym of previous party, party code according to the CMP dataset, Pathways identifier for the previous party; number of years of formal affiliation to the previous party; year in which the MP joined a political party for the first time; membership in  committees ; total number of parliamentary committees in which the MPs was a member during this legislative term; name of the committee joined for the longest period in this term; role or position in committee 1 and 2; total number of months as a member of committee during this legislative term; member of this committee during a prior legislative term; type of committee; relevance of committee; immigration related committee; names of all other committees that the MP joined during legislative term; membership of any all-party group (APG), friendship group or issue group mentioned in the personal (parliamentary) website; MP has served during this term as Chair, Speaker, President of the Chamber, as Deputy Chair, Deputy Speaker, Deputy President of the Chamber, as a member of the political administration/management team of the Chamber, as a member of the parliamentary party leadership group.\nAdditionally coded was: other observations; MP, MPs mother and father expelled; name and surname Greek; titular MP; membership and time in government."
  },
  "availability" : {
    "availabilityType" : "Download",
    "embargoDate" : "",
    "label" : [ {
      "pref" : "A - Daten und Dokumente sind für die akademische Forschung und Lehre freigegeben.",
      "de" : "A - Daten und Dokumente sind für die akademische Forschung und Lehre freigegeben.",
      "en" : "A - Data and documents are released for academic research and teaching."
    } ]
  },
  "collectionModes" : [ {
    "label" : {
      "pref" : "Inhaltscodierung\n\nBestehende Datensätze aus anderen Forschungsprojekten, Webseiten nationaler und regionaler Parlamente, Veröffentlichungen der nationalen und regionalen Parlamente, nationale und regionale Verbände ehemaliger Abgeordneter, Internetseiten von parlamentarischen Gruppen / nationale und regionale Organisationen politischer Parteien, Persönliche Blogs / Webseiten / Social Media Profile von Abgeordneten und andere Quellen.",
      "de" : "Inhaltscodierung\n\nBestehende Datensätze aus anderen Forschungsprojekten, Webseiten nationaler und regionaler Parlamente, Veröffentlichungen der nationalen und regionalen Parlamente, nationale und regionale Verbände ehemaliger Abgeordneter, Internetseiten von parlamentarischen Gruppen / nationale und regionale Organisationen politischer Parteien, Persönliche Blogs / Webseiten / Social Media Profile von Abgeordneten und andere Quellen.",
      "en" : "Content Coding\n\nExisting datasets from other research projects, websites of national and regional Parliaments, publications of the national and regional Parliaments, national and regional associations of former MPs, websites of parliamentary groups/national and regional organizations of political parties, Personal blogs/webpages/social media profiles of MPs, and other sources."
    }
  } ],
  "contributions" : [ {
    "type" : [ "Creator" ],
    "agent" : {
      "type" : [ "Institution" ],
      "uri" : "info:searchapp/vocab/adhoc/VmFuIEhhdXdhZXJ0LCBTdGV2ZW4gTS47IEphbnNzZW4sIENobG-DqQ==",
      "label" : "Van Hauwaert, Steven M.; Janssen, Chloé"
    },
    "role" : [ ]
  } ],
  "describedBy" : {
    "uri" : "https://git.gesis.org/datorium/dbk-oai-dprex-set-data/oai.dbk.gesis.org.DBK.ZA6796",
    "type" : "ResearchData",
    "modifiedBy" : {
      "uri" : "https://searchapp-indexer",
      "label" : {
        "pref" : "Searchapp Indexer"
      },
      "dateCreated" : "2024-06-21T05:26:25.61+02:00",
      "dateModified" : "2024-06-21T05:26:25.61+02:00",
      "inDataset" : {
        "uri" : "https://dbkapps.gesis.org/dbkoai/?verb=ListIdentifiers&metadataPrefix=oai_dara&set=DPREX"
      },
      "resultOf" : {
        "type" : [ "CreateAction" ],
        "endTime" : "2024-06-21T05:26:25.61+02:00",
        "instrument" : {
          "uri" : "https://git.gesis.org/datorium/searchapp-indexer",
          "type" : [ "SoftwareApplication" ],
          "label" : "Searchapp Indexer"
        },
        "srcObject" : {
          "uri" : "https://dbkapps.gesis.org/dbkoai/?verb=GetRecord&metadataPrefix=oai_dara&identifier=oai:dbk.gesis.org:DBK/ZA6796",
          "type" : [ "DataFeedItem" ],
          "inDataset" : {
            "uri" : "https://dbkapps.gesis.org/dbkoai/?verb=ListIdentifiers&metadataPrefix=oai_dara&set=DPREX",
            "label" : "DBK_OAI"
          }
        }
      }
    }
  },
  "documentId" : "oai.dbk.gesis.org.DBK.ZA6796",
  "geographicCoverages" : [ {
    "geographicCoverageControlled" : "BE"
  } ],
  "handles" : [ {
    "type" : "DOI",
    "notation" : "10.4232/1.12793",
    "url" : "https://doi.org/10.4232/1.12793",
    "source" : {
      "uri" : "https://www.da-ra.de/",
      "label" : "DARA"
    }
  } ],
  "natureOfContents" : [ {
    "uri" : "info:searchapp/vocab/Dataset",
    "label" : {
      "pref" : "Dataset",
      "de" : "Dataset",
      "en" : "Dataset"
    }
  } ],
  "publications" : [ {
    "type" : [ "PublicationEvent" ],
    "startDate" : "2017"
  } ],
  "resourceLanguage" : "deu",
  "rights" : {
    "licenseType" : "",
    "label" : {
      "pref" : "Alle im GESIS DBK veröffentlichten Metadaten sind frei verfügbar unter den Creative Commons CC0 1.0 Universal Public Domain Dedication. GESIS bittet jedoch darum, dass Sie alle Metadatenquellen anerkennen und sie nennen, etwa die Datengeber oder jeglichen Aggregator, inklusive GESIS selbst. Für weitere Informationen siehe https://dbk.gesis.org/dbksearch/guidelines.asp?db=d",
      "de" : "Alle im GESIS DBK veröffentlichten Metadaten sind frei verfügbar unter den Creative Commons CC0 1.0 Universal Public Domain Dedication. GESIS bittet jedoch darum, dass Sie alle Metadatenquellen anerkennen und sie nennen, etwa die Datengeber oder jeglichen Aggregator, inklusive GESIS selbst. Für weitere Informationen siehe https://dbk.gesis.org/dbksearch/guidelines.asp?db=d",
      "en" : "All metadata from GESIS DBK are available free of restriction under the Creative Commons CC0 1.0 Universal Public Domain Dedication. However, GESIS requests that you actively acknowledge and give attribution to all metadata sources, such as the data providers and any data aggregators, including GESIS. For further information see https://dbk.gesis.org/dbksearch/guidelines.asp"
    }
  },
  "sampling" : {
    "method" : {
      "pref" : "Auswahlverfahren Kommentar: Vollerhebung",
      "de" : "Auswahlverfahren Kommentar: Vollerhebung",
      "en" : "Sampling Procedure Comment: Total universe/Complete Enumeration"
    }
  },
  "subjects" : [ {
    "type" : [ "SubjectHeading" ],
    "uri" : "info:searchapp/vocab/adhoc/QVJCRUlUIFVORCBCRVNDSMOERlRJR1VORw==",
    "notation" : "",
    "source" : {
      "uri" : "info:searchapp/vocab/adhoc/Q0VTU0RBIFRvcGljIENsYXNzaWZpY2F0aW9u",
      "label" : "CESSDA Topic Classification"
    },
    "label" : {
      "pref" : "ARBEIT UND BESCHÄFTIGUNG",
      "de" : "ARBEIT UND BESCHÄFTIGUNG",
      "en" : "LABOUR AND EMPLOYMENT"
    }
  }, {
    "type" : [ "SubjectHeading" ],
    "uri" : "info:searchapp/vocab/adhoc/TWlncmF0aW9u",
    "notation" : "",
    "source" : {
      "uri" : "info:searchapp/vocab/adhoc/Q0VTU0RBIFRvcGljIENsYXNzaWZpY2F0aW9u",
      "label" : "CESSDA Topic Classification"
    },
    "label" : {
      "pref" : "Migration",
      "de" : "Migration",
      "en" : "Migration"
    }
  }, {
    "type" : [ "SubjectHeading" ],
    "uri" : "info:searchapp/vocab/adhoc/UmVnaWVydW5nLCBwb2xpdGlzY2hlIFN5c3RlbWUsIFBhcnRlaWVuIHVuZCBPcmdhbmlzYXRpb25lbg==",
    "notation" : "",
    "source" : {
      "uri" : "info:searchapp/vocab/adhoc/Q0VTU0RBIFRvcGljIENsYXNzaWZpY2F0aW9u",
      "label" : "CESSDA Topic Classification"
    },
    "label" : {
      "pref" : "Regierung, politische Systeme, Parteien und Organisationen",
      "de" : "Regierung, politische Systeme, Parteien und Organisationen",
      "en" : "Government, political systems and organisations"
    }
  } ],
  "temporalCoverages" : [ {
    "startDate" : "1991",
    "endDate" : "2010",
    "labels" : [ ]
  } ],
  "timeDimensions" : [ {
    "timeDimensionType" : ""
  } ],
  "title" : {
    "pref" : "Pathways to Power: The Political Representation of Citizens of Immigrant Origin in Belgium (BE-PATHWAYS)",
    "de" : "Pathways to Power: The Political Representation of Citizens of Immigrant Origin in Belgium (BE-PATHWAYS)",
    "en" : "Pathways to Power: The Political Representation of Citizens of Immigrant Origin in Belgium (BE-PATHWAYS)"
  },
  "universe" : {
    "sampled" : {
      "pref" : "Alle Abgeordneten des belgischen nationalen Parlaments und regionaler Parlamente während der Legislaturperioden 1991-2010.\n\nNationale Daten werden für alle Abgeordneten (einschließlich der Abgeordneten ohne Migrationshintergrund) gesammelt, die in die unteren Kammer für alle Legislaturperioden seit 1990 gewählt wurden. Darüber hinaus werden regionale Daten gesammelt, einschließlich aller Abgeordneten aus der jüngsten Legislaturperiode vor dem Beginn der Datenerfassung in jeder Region.",
      "de" : "Alle Abgeordneten des belgischen nationalen Parlaments und regionaler Parlamente während der Legislaturperioden 1991-2010.\n\nNationale Daten werden für alle Abgeordneten (einschließlich der Abgeordneten ohne Migrationshintergrund) gesammelt, die in die unteren Kammer für alle Legislaturperioden seit 1990 gewählt wurden. Darüber hinaus werden regionale Daten gesammelt, einschließlich aller Abgeordneten aus der jüngsten Legislaturperiode vor dem Beginn der Datenerfassung in jeder Region.",
      "en" : "All Member of Parliament (MPs) of the Belgian national parliament and regional parliaments during the legislative periods 1991-2010.\n\nNational level data is being collected for all MPs (including those not of immigrant origin) elected in the Lower Chamber for all legislative terms since 1990. Additionally, regional data is being collected, including all MPs from the most recent term that ended before the start of the data collection in each region."
    }
  },
  "uri" : "https://doi.org/10.4232/1.12793",
  "searchappPublisher" : {
    "shortName" : "GESIS",
    "name" : {
      "de" : "GESIS - Leibniz Institut für Sozialwissenschaften",
      "en" : "GESIS - Leibniz Institute for the Social Sciences"
    },
    "url" : "https://www.gesis.org/"
  },
  "calculatedTemporalCoverage" : {
    "label" : "1991 - 2010",
    "dates" : [ "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010" ]
  }
}
