{
  "abstract" : {
    "pref" : "Das vorliegende Datenhandbuch soll der Forschung zu den Aspekten internationaler Migration das adäquate Datenmaterial an die Hand geben. Anregung für diese Datensammlung wurde 1924 durch das Komitee zu den wissenschaftlichen Aspekten menschlicher Wanderung des Social Science Research Council gegeben. Die Durchführung der statistischen Studie wurde dem National Bureau of Economic Research (NBER) in New York (Prof. Dr. Willcox) anvertraut, welches unterstützt wurde von der Abteilung Migration des International Labour Office (ILO bzw. Internationale Arbeitsorganisation (IAO), Prof. Dr. Ferenczi) in Genf (Schweiz). Das vorliegende Datenhandbuch geht über die Zusammenstellung bekannter, vorliegender Statistiken der einzelnen Länder hinaus. Viele Materialien wurden neben den schon publizierten öffentlichen Statistiken in den Archiven zusätzlich gesichtet und aufbereitet. \nDie Forscher sammelten nationale Statistiken und stellten sie in internationalen Tabellen zusammen, soweit es die Datenlage erlaubte. Die besondere Herausforderung dieser Arbeit lag in der Tatsache, dass die Unvollständigkeit der nationalen Migrationsstatistiken steigt, je weiter die Daten in die Vergangenheit zurückreichen. Für jedes Land wurde die Anzahl der Auswanderer nach dem von ihnen angegebenen Zielland bzw. Einwanderungsland erhoben. Weiterhin wurden im Gegenzug für jedes Land die Einwanderer nach dem Land ihrer Abfahrt erfasst. Damit sollte für jedes Land ein Überblick der dieses Land betreffenden Migrationsflüsse erstellt werden. Interkontinentale Wanderungsbewegungen stellen den Schwerpunkt dieser Studie dar. Die kontinentale Wanderung innerhalb Europas und anderen Teilen der Welt wurde jedoch ebenfalls erfasst. \nDas Material für die Statistiken wurde beschafft durch die Korrespondenz mit dem ILO und seinen Mitglieds-Staaten (Vereinte Nationen), durch die Zusammenarbeit mit den statistischen Ämtern der jeweiligen Länder und durch Sichtung der Archive.\nIn den nationalen Datentabellen werden die Migranten zum Zeitpunkt ihrer Abreise aus dem Land ihres gegenwärtigen Aufenthalts bzw. zum Zeitpunkt ihrer Ankunft in dem Land ihres zukünftigen Aufenthaltes erfasst. Bevölkerungsstatistiken oder Arbeitsmarktstatistiken, in denen auch die ausländische Bevölkerung erfasst wird und die daher eine indirekte Schlussfolgerung auf Wanderungsbewegungen zulassen, sind von den Autoren nicht berücksichtigt worden. (Ferenczi und Willcox, 1969, S. 67) Dort, wo Migrationsstatistiken auf der Basis von unterschiedlichen Methoden erhoben wurden, wie z.B. Hafen-Statistiken, Reisepass-Statistiken, oder Grenz-Statistiken, sind die entsprechenden Werte berücksichtigt worden und in den Tabellen wurde auf die Quelle hingewiesen. Dort, wo in den nationalen Migrationsstatistiken Auswanderer nach dem Zielland oder Einwanderer nach ihrem Herkunftsland klassifiziert wurden, wird sich in der Statistik indirekt auf ein anderes Land bezogen. Für die jeweiligen anderen Länder, welche in diesen nationalen Datentabellen erwähnt werden, stellen diese Statistiken eine Art ’indirekte Wanderungsstatistik‘ dar. Indirekte Statistiken beziehen sich normalerweise auf die Nennung von Ländern (z.B. Herkunftsland). Dort, wo diese Nennungen fehlen, wurde die Nationalität oder die Volkszugehörigkeit der Migranten herangezogen. Weiterhin wird in den Daten zwischen Bürgern des Landes (Dänen, d.h. in Dänemark geborene Bürger) und Ausländern unterschieden. Hierbei wurde immer die Definition des jeweiligen Landes für Staatsbürger und für Ausländer bei der Datenerhebung herangezogen. (Ferenczi und Willcox, 1969, S. 67) Ebenfalls wurde zwischen kontinentalen und interkontinentalen Migrationsbewegungen unterschieden. Eine Migration wird als kontinental bezeichnet, wenn sie zwischen den Territorien verschiedener Länder des gleichen Kontinents stattfindet. Sie wird als interkontinental bezeichnet, wenn Länder unterschiedlicher Kontinente betroffen sind. (Ferenczi und Willcox, 1969, S. 68) Als Regel geben die Autoren folgende persönliche Charakteristiken der Migranten an: Geschlecht, Alter, Nationalität, Beruf, Land des letzten ständigen Aufenthaltsortes und das Land des zukünftigen ständigen Aufenthaltsortes. Diese Eigenschaften wurden auf der Basis der ’International Labour Conference‘ von 1922, Empfehlung Nr. 19, gewählt. Für Migrations-Statistiken sind die staatlichen Territorien von besonderer Bedeutung. Historische Grenzverläufe und ihre Veränderungen über die Zeit sind von besonderer Bedeutung. So ist es z.B. irreführend, den heutigen Begriff des ‚Vereinten Königreichs von England‘ (United Kingdom) zu verwenden, da seine heutige Bedeutung durch die Etablierung des Freien Irischen Staates sich verändert hat. Daher wird der Begriff ‚Britische Inseln‘ von den Autoren verwendet. Dort, wo sich historische Territorien über die Zeit verändert haben, wurde das neue Territorium in der Hauptüberschrift und das ältere Territorium unterhalb der Hauptüberschrift genannt (z.B.: Ungarn – vor dem Krieg und nach dem Krieg; Irish Free State – Ireland, etc.) (Ferenczi und Willcox, 1969, S. 68) Wo frühere Territorien aufgehört haben, ein selbständiges politisches oder administratives Gebiet zu sein, wurde es unter dem früheren vorherrschenden Gebiet klassifiziert (z.B. wurden Bosnien und Herzegovina unter Österreich plaziert). In allen Tabellen werden die Migranten in 12-Monats-Perioden dargestellt, soweit es möglich war. Rechnungsjahre wurden meistens von Kalenderjahren getrennt dargestellt, wobei eine Information über die exakte Periode des Rechnungsjahres in den Anmerkungen gegeben wurde. Wo Statistiken nur für Fünfjahres- oder Zehnjahres-Zeiträume vorlagen, wurde in den Originalquellen nach den jeweiligen Jahresdaten recherchiert. Es kamen für die Studie nur Statistiken offizieller Quellen zur Anwendung. Nur in seltenen Fällen wurde auf sekundäre Quellen zurückgegriffen (Briefe, offizielle Korrespondenzen). Der Vorzug wurde den offiziellen Statistiken mit dem spätesten Datum gegeben. Die Nationalen Statistiken berichten die Berufe in der Klassifikation, die in den Quellen verwendet wurde. Wo möglich, wurde die Untergliederung mit den sechs Klassen ’Landwirtschaft‘, ’Industrie und Bergbau‘, ’Transport und Handel bzw. Kommunikation‘, ’Hausdienstleistungen und Handwerk‘, ’freie Berufe und öffentliche Dienstleistungen‘, sowie ’andere Berufe, keinen Beruf, Beruf unbekannt‘ gewählt. Familienmitglieder, die nicht berufstätig waren, wurden in Kategorie 6 (andere Berufe, keinen Beruf, Beruf unbekannt) eingeordnet. (Ferenczi und Willcox, 1969, S. 70)In den nationalen Datentabellen, in denen die Einwanderer nach dem Land des letzten ständigen Aufenthaltsortes oder nach ihrer Nationalität aufgeführt werden, wurde meistens die Klassifikation der genutzten offiziellen Quelle des jeweiligen Landes beibehalten, wobei die genutzte Klassifikation der USA als Arbeitsgrundlage diente. Wenn die nationalen Untergliederungen sehr viel mehr Klassifikationen hatten als jede der USA, wurden diese Untergliederungen den größeren Gruppen der US-Klassifikation angepasst. Wo es schwierig war, ein Territorium einem Land zuzuordnen, wurde die Klassifikation des ’International Statistical Institute‘ (ISI) herangezogen. In anderen Fällen wurde die Nationalität oder die Volkszugehörigkeit nach geographischen oder politischen Gesichtspunkten gewählt (z.B.: Juden (nicht spezifiziert) wurden unter den Gruppen ‚andere Europäer‘ aufgeführt. Juden (polnisch) wurden unter ‚Polen‘ aufgeführt. Türken (nicht spezifiziert) wurden unter ‚Türken in Asien‘ aufgeführt, etc.). (Ferenczi und Willcox, 1969, S. 70) \nDänemarkAuswanderungsstatistiken nach Überseeländern wurden gemäß der Anordnung des Gesetzes vom 1. Mai 1868 zur Erfassung von Transport und Auswanderung sowie auf Grundlage einer Anordnung des Justizministers von 1873 erstellt. Es mussten genaue Angaben über die Auswanderer sowie über ihr Ziel der Auswanderung erfasst werden. (Ferenczi und Willcox, 1969, S. 666)Die vorliegenden Tabellen enthalten die Zusammenstellung der dänischen Migrationsstatistik. Folgende Themen werden hier behandelt:\nA.01 Auswanderer in nicht-europäische Länder nach Alter und Geschlecht, 1820-1868\nA.02 Männliche Auswanderer ab 15 Jahre und älter nach Beruf, 1872-1924 \nA.03 Auswanderung nach Ziel der Auswanderung, 1869-1924 \nDie Werte für auswandernde Dänen nach Argentinien, Kanada, USA und Australien sowie die Angaben über rückkehrende Dänen von Argentinien und den USA sind in den jeweiligen nationalen Tabellen Argentiniens, Kanadas, der USA und Australiens zu finden.",
    "de" : "Das vorliegende Datenhandbuch soll der Forschung zu den Aspekten internationaler Migration das adäquate Datenmaterial an die Hand geben. Anregung für diese Datensammlung wurde 1924 durch das Komitee zu den wissenschaftlichen Aspekten menschlicher Wanderung des Social Science Research Council gegeben. Die Durchführung der statistischen Studie wurde dem National Bureau of Economic Research (NBER) in New York (Prof. Dr. Willcox) anvertraut, welches unterstützt wurde von der Abteilung Migration des International Labour Office (ILO bzw. Internationale Arbeitsorganisation (IAO), Prof. Dr. Ferenczi) in Genf (Schweiz). Das vorliegende Datenhandbuch geht über die Zusammenstellung bekannter, vorliegender Statistiken der einzelnen Länder hinaus. Viele Materialien wurden neben den schon publizierten öffentlichen Statistiken in den Archiven zusätzlich gesichtet und aufbereitet. \nDie Forscher sammelten nationale Statistiken und stellten sie in internationalen Tabellen zusammen, soweit es die Datenlage erlaubte. Die besondere Herausforderung dieser Arbeit lag in der Tatsache, dass die Unvollständigkeit der nationalen Migrationsstatistiken steigt, je weiter die Daten in die Vergangenheit zurückreichen. Für jedes Land wurde die Anzahl der Auswanderer nach dem von ihnen angegebenen Zielland bzw. Einwanderungsland erhoben. Weiterhin wurden im Gegenzug für jedes Land die Einwanderer nach dem Land ihrer Abfahrt erfasst. Damit sollte für jedes Land ein Überblick der dieses Land betreffenden Migrationsflüsse erstellt werden. Interkontinentale Wanderungsbewegungen stellen den Schwerpunkt dieser Studie dar. Die kontinentale Wanderung innerhalb Europas und anderen Teilen der Welt wurde jedoch ebenfalls erfasst. \nDas Material für die Statistiken wurde beschafft durch die Korrespondenz mit dem ILO und seinen Mitglieds-Staaten (Vereinte Nationen), durch die Zusammenarbeit mit den statistischen Ämtern der jeweiligen Länder und durch Sichtung der Archive.\nIn den nationalen Datentabellen werden die Migranten zum Zeitpunkt ihrer Abreise aus dem Land ihres gegenwärtigen Aufenthalts bzw. zum Zeitpunkt ihrer Ankunft in dem Land ihres zukünftigen Aufenthaltes erfasst. Bevölkerungsstatistiken oder Arbeitsmarktstatistiken, in denen auch die ausländische Bevölkerung erfasst wird und die daher eine indirekte Schlussfolgerung auf Wanderungsbewegungen zulassen, sind von den Autoren nicht berücksichtigt worden. (Ferenczi und Willcox, 1969, S. 67) Dort, wo Migrationsstatistiken auf der Basis von unterschiedlichen Methoden erhoben wurden, wie z.B. Hafen-Statistiken, Reisepass-Statistiken, oder Grenz-Statistiken, sind die entsprechenden Werte berücksichtigt worden und in den Tabellen wurde auf die Quelle hingewiesen. Dort, wo in den nationalen Migrationsstatistiken Auswanderer nach dem Zielland oder Einwanderer nach ihrem Herkunftsland klassifiziert wurden, wird sich in der Statistik indirekt auf ein anderes Land bezogen. Für die jeweiligen anderen Länder, welche in diesen nationalen Datentabellen erwähnt werden, stellen diese Statistiken eine Art ’indirekte Wanderungsstatistik‘ dar. Indirekte Statistiken beziehen sich normalerweise auf die Nennung von Ländern (z.B. Herkunftsland). Dort, wo diese Nennungen fehlen, wurde die Nationalität oder die Volkszugehörigkeit der Migranten herangezogen. Weiterhin wird in den Daten zwischen Bürgern des Landes (Dänen, d.h. in Dänemark geborene Bürger) und Ausländern unterschieden. Hierbei wurde immer die Definition des jeweiligen Landes für Staatsbürger und für Ausländer bei der Datenerhebung herangezogen. (Ferenczi und Willcox, 1969, S. 67) Ebenfalls wurde zwischen kontinentalen und interkontinentalen Migrationsbewegungen unterschieden. Eine Migration wird als kontinental bezeichnet, wenn sie zwischen den Territorien verschiedener Länder des gleichen Kontinents stattfindet. Sie wird als interkontinental bezeichnet, wenn Länder unterschiedlicher Kontinente betroffen sind. (Ferenczi und Willcox, 1969, S. 68) Als Regel geben die Autoren folgende persönliche Charakteristiken der Migranten an: Geschlecht, Alter, Nationalität, Beruf, Land des letzten ständigen Aufenthaltsortes und das Land des zukünftigen ständigen Aufenthaltsortes. Diese Eigenschaften wurden auf der Basis der ’International Labour Conference‘ von 1922, Empfehlung Nr. 19, gewählt. Für Migrations-Statistiken sind die staatlichen Territorien von besonderer Bedeutung. Historische Grenzverläufe und ihre Veränderungen über die Zeit sind von besonderer Bedeutung. So ist es z.B. irreführend, den heutigen Begriff des ‚Vereinten Königreichs von England‘ (United Kingdom) zu verwenden, da seine heutige Bedeutung durch die Etablierung des Freien Irischen Staates sich verändert hat. Daher wird der Begriff ‚Britische Inseln‘ von den Autoren verwendet. Dort, wo sich historische Territorien über die Zeit verändert haben, wurde das neue Territorium in der Hauptüberschrift und das ältere Territorium unterhalb der Hauptüberschrift genannt (z.B.: Ungarn – vor dem Krieg und nach dem Krieg; Irish Free State – Ireland, etc.) (Ferenczi und Willcox, 1969, S. 68) Wo frühere Territorien aufgehört haben, ein selbständiges politisches oder administratives Gebiet zu sein, wurde es unter dem früheren vorherrschenden Gebiet klassifiziert (z.B. wurden Bosnien und Herzegovina unter Österreich plaziert). In allen Tabellen werden die Migranten in 12-Monats-Perioden dargestellt, soweit es möglich war. Rechnungsjahre wurden meistens von Kalenderjahren getrennt dargestellt, wobei eine Information über die exakte Periode des Rechnungsjahres in den Anmerkungen gegeben wurde. Wo Statistiken nur für Fünfjahres- oder Zehnjahres-Zeiträume vorlagen, wurde in den Originalquellen nach den jeweiligen Jahresdaten recherchiert. Es kamen für die Studie nur Statistiken offizieller Quellen zur Anwendung. Nur in seltenen Fällen wurde auf sekundäre Quellen zurückgegriffen (Briefe, offizielle Korrespondenzen). Der Vorzug wurde den offiziellen Statistiken mit dem spätesten Datum gegeben. Die Nationalen Statistiken berichten die Berufe in der Klassifikation, die in den Quellen verwendet wurde. Wo möglich, wurde die Untergliederung mit den sechs Klassen ’Landwirtschaft‘, ’Industrie und Bergbau‘, ’Transport und Handel bzw. Kommunikation‘, ’Hausdienstleistungen und Handwerk‘, ’freie Berufe und öffentliche Dienstleistungen‘, sowie ’andere Berufe, keinen Beruf, Beruf unbekannt‘ gewählt. Familienmitglieder, die nicht berufstätig waren, wurden in Kategorie 6 (andere Berufe, keinen Beruf, Beruf unbekannt) eingeordnet. (Ferenczi und Willcox, 1969, S. 70)In den nationalen Datentabellen, in denen die Einwanderer nach dem Land des letzten ständigen Aufenthaltsortes oder nach ihrer Nationalität aufgeführt werden, wurde meistens die Klassifikation der genutzten offiziellen Quelle des jeweiligen Landes beibehalten, wobei die genutzte Klassifikation der USA als Arbeitsgrundlage diente. Wenn die nationalen Untergliederungen sehr viel mehr Klassifikationen hatten als jede der USA, wurden diese Untergliederungen den größeren Gruppen der US-Klassifikation angepasst. Wo es schwierig war, ein Territorium einem Land zuzuordnen, wurde die Klassifikation des ’International Statistical Institute‘ (ISI) herangezogen. In anderen Fällen wurde die Nationalität oder die Volkszugehörigkeit nach geographischen oder politischen Gesichtspunkten gewählt (z.B.: Juden (nicht spezifiziert) wurden unter den Gruppen ‚andere Europäer‘ aufgeführt. Juden (polnisch) wurden unter ‚Polen‘ aufgeführt. Türken (nicht spezifiziert) wurden unter ‚Türken in Asien‘ aufgeführt, etc.). (Ferenczi und Willcox, 1969, S. 70) \nDänemarkAuswanderungsstatistiken nach Überseeländern wurden gemäß der Anordnung des Gesetzes vom 1. Mai 1868 zur Erfassung von Transport und Auswanderung sowie auf Grundlage einer Anordnung des Justizministers von 1873 erstellt. Es mussten genaue Angaben über die Auswanderer sowie über ihr Ziel der Auswanderung erfasst werden. (Ferenczi und Willcox, 1969, S. 666)Die vorliegenden Tabellen enthalten die Zusammenstellung der dänischen Migrationsstatistik. Folgende Themen werden hier behandelt:\nA.01 Auswanderer in nicht-europäische Länder nach Alter und Geschlecht, 1820-1868\nA.02 Männliche Auswanderer ab 15 Jahre und älter nach Beruf, 1872-1924 \nA.03 Auswanderung nach Ziel der Auswanderung, 1869-1924 \nDie Werte für auswandernde Dänen nach Argentinien, Kanada, USA und Australien sowie die Angaben über rückkehrende Dänen von Argentinien und den USA sind in den jeweiligen nationalen Tabellen Argentiniens, Kanadas, der USA und Australiens zu finden.",
    "en" : ""
  },
  "availability" : {
    "availabilityType" : "Download",
    "embargoDate" : "",
    "label" : [ {
      "pref" : "0 - Daten und Dokumente sind für jedermann freigegeben.",
      "de" : "0 - Daten und Dokumente sind für jedermann freigegeben.",
      "en" : "0 - Data and documents are released for everybody."
    } ]
  },
  "contributions" : [ {
    "type" : [ "Creator" ],
    "agent" : {
      "type" : [ "Person" ],
      "uri" : "info:searchapp/vocab/adhoc/RmVyZW5jemksIEltcmU=",
      "label" : "Ferenczi, Imre",
      "affiliation" : {
        "uri" : "info:searchapp/vocab/adhoc/SW50ZXJuYXRpb25hbCBMYWJvdXIgT2ZmaWNlIChJTE8pLCBBYnQuIE1pZ3JhdGlvbg==",
        "label" : {
          "pref" : "International Labour Office (ILO), Abt. Migration",
          "de" : "International Labour Office (ILO), Abt. Migration",
          "en" : "International Labour Office (ILO), Abt. Migration"
        }
      }
    },
    "role" : [ ]
  }, {
    "type" : [ "Creator" ],
    "agent" : {
      "type" : [ "Person" ],
      "uri" : "info:searchapp/vocab/adhoc/V2lsbGNveCwgV2FsdGVyIEYu",
      "label" : "Willcox, Walter F.",
      "affiliation" : {
        "uri" : "info:searchapp/vocab/adhoc/TmF0aW9uYWwgQnVyZWF1IG9mIEVjb25vbWljIFJlc2VhcmNoIChOQkVSKQ==",
        "label" : {
          "pref" : "National Bureau of Economic Research (NBER)",
          "de" : "National Bureau of Economic Research (NBER)",
          "en" : "National Bureau of Economic Research (NBER)"
        }
      }
    },
    "role" : [ ]
  } ],
  "describedBy" : {
    "uri" : "https://git.gesis.org/datorium/dbk-oai-dprex-set-data/oai.dbk.gesis.org.DBK.ZA8660",
    "type" : "ResearchData",
    "modifiedBy" : {
      "uri" : "https://searchapp-indexer",
      "label" : {
        "pref" : "Searchapp Indexer"
      },
      "dateCreated" : "2024-06-21T05:26:26.049+02:00",
      "dateModified" : "2024-06-21T05:26:26.049+02:00",
      "inDataset" : {
        "uri" : "https://dbkapps.gesis.org/dbkoai/?verb=ListIdentifiers&metadataPrefix=oai_dara&set=DPREX"
      },
      "resultOf" : {
        "type" : [ "CreateAction" ],
        "endTime" : "2024-06-21T05:26:26.049+02:00",
        "instrument" : {
          "uri" : "https://git.gesis.org/datorium/searchapp-indexer",
          "type" : [ "SoftwareApplication" ],
          "label" : "Searchapp Indexer"
        },
        "srcObject" : {
          "uri" : "https://dbkapps.gesis.org/dbkoai/?verb=GetRecord&metadataPrefix=oai_dara&identifier=oai:dbk.gesis.org:DBK/ZA8660",
          "type" : [ "DataFeedItem" ],
          "inDataset" : {
            "uri" : "https://dbkapps.gesis.org/dbkoai/?verb=ListIdentifiers&metadataPrefix=oai_dara&set=DPREX",
            "label" : "DBK_OAI"
          }
        }
      }
    }
  },
  "documentId" : "oai.dbk.gesis.org.DBK.ZA8660",
  "geographicCoverages" : [ {
    "geographicCoverageControlled" : "DK"
  } ],
  "handles" : [ {
    "type" : "DOI",
    "notation" : "10.4232/1.12891",
    "url" : "https://doi.org/10.4232/1.12891",
    "source" : {
      "uri" : "https://www.da-ra.de/",
      "label" : "DARA"
    }
  } ],
  "natureOfContents" : [ {
    "uri" : "info:searchapp/vocab/Dataset",
    "label" : {
      "pref" : "Dataset",
      "de" : "Dataset",
      "en" : "Dataset"
    }
  } ],
  "publications" : [ {
    "type" : [ "PublicationEvent" ],
    "startDate" : "2017"
  } ],
  "resourceLanguage" : "deu",
  "rights" : {
    "licenseType" : "",
    "label" : {
      "pref" : "Alle im GESIS DBK veröffentlichten Metadaten sind frei verfügbar unter den Creative Commons CC0 1.0 Universal Public Domain Dedication. GESIS bittet jedoch darum, dass Sie alle Metadatenquellen anerkennen und sie nennen, etwa die Datengeber oder jeglichen Aggregator, inklusive GESIS selbst. Für weitere Informationen siehe https://dbk.gesis.org/dbksearch/guidelines.asp?db=d",
      "de" : "Alle im GESIS DBK veröffentlichten Metadaten sind frei verfügbar unter den Creative Commons CC0 1.0 Universal Public Domain Dedication. GESIS bittet jedoch darum, dass Sie alle Metadatenquellen anerkennen und sie nennen, etwa die Datengeber oder jeglichen Aggregator, inklusive GESIS selbst. Für weitere Informationen siehe https://dbk.gesis.org/dbksearch/guidelines.asp?db=d",
      "en" : "All metadata from GESIS DBK are available free of restriction under the Creative Commons CC0 1.0 Universal Public Domain Dedication. However, GESIS requests that you actively acknowledge and give attribution to all metadata sources, such as the data providers and any data aggregators, including GESIS. For further information see https://dbk.gesis.org/dbksearch/guidelines.asp"
    }
  },
  "sampling" : {
    "method" : {
      "pref" : "Auswahlverfahren Kommentar: Amtliche Quellen, Die Auswanderungs- und Einwanderungsstatistiken der jeweiligen Länder.",
      "de" : "Auswahlverfahren Kommentar: Amtliche Quellen, Die Auswanderungs- und Einwanderungsstatistiken der jeweiligen Länder.",
      "en" : ""
    }
  },
  "subjects" : [ {
    "type" : [ "SubjectHeading" ],
    "uri" : "info:searchapp/vocab/adhoc/R0VTQ0hJQ0hURQ==",
    "notation" : "",
    "source" : {
      "uri" : "info:searchapp/vocab/adhoc/Q0VTU0RBIFRvcGljIENsYXNzaWZpY2F0aW9u",
      "label" : "CESSDA Topic Classification"
    },
    "label" : {
      "pref" : "GESCHICHTE",
      "de" : "GESCHICHTE",
      "en" : "HISTORY"
    }
  }, {
    "type" : [ "SubjectHeading" ],
    "uri" : "info:searchapp/vocab/adhoc/TWlncmF0aW9u",
    "notation" : "",
    "source" : {
      "uri" : "info:searchapp/vocab/adhoc/Q0VTU0RBIFRvcGljIENsYXNzaWZpY2F0aW9u",
      "label" : "CESSDA Topic Classification"
    },
    "label" : {
      "pref" : "Migration",
      "de" : "Migration",
      "en" : "Migration"
    }
  } ],
  "temporalCoverages" : [ {
    "startDate" : "1816-01-01",
    "endDate" : "1924-12-31",
    "labels" : [ ]
  } ],
  "timeDimensions" : [ {
    "timeDimensionType" : ""
  } ],
  "title" : {
    "pref" : "Nationale Tabellen der internationalen Migrations-Statistik: Dänemark, 1816-1924",
    "de" : "Nationale Tabellen der internationalen Migrations-Statistik: Dänemark, 1816-1924",
    "en" : "National Tables of International Migration Statistics: Denmark, 1816-1924"
  },
  "universe" : { },
  "uri" : "https://doi.org/10.4232/1.12891",
  "searchappPublisher" : {
    "shortName" : "GESIS",
    "name" : {
      "de" : "GESIS - Leibniz Institut für Sozialwissenschaften",
      "en" : "GESIS - Leibniz Institute for the Social Sciences"
    },
    "url" : "https://www.gesis.org/"
  },
  "calculatedTemporalCoverage" : {
    "label" : "1816 - 1924",
    "dates" : [ "1816", "1817", "1818", "1819", "1820", "1821", "1822", "1823", "1824", "1825", "1826", "1827", "1828", "1829", "1830", "1831", "1832", "1833", "1834", "1835", "1836", "1837", "1838", "1839", "1840", "1841", "1842", "1843", "1844", "1845", "1846", "1847", "1848", "1849", "1850", "1851", "1852", "1853", "1854", "1855", "1856", "1857", "1858", "1859", "1860", "1861", "1862", "1863", "1864", "1865", "1866", "1867", "1868", "1869", "1870", "1871", "1872", "1873", "1874", "1875", "1876", "1877", "1878", "1879", "1880", "1881", "1882", "1883", "1884", "1885", "1886", "1887", "1888", "1889", "1890", "1891", "1892", "1893", "1894", "1895", "1896", "1897", "1898", "1899", "1900", "1901", "1902", "1903", "1904", "1905", "1906", "1907", "1908", "1909", "1910", "1911", "1912", "1913", "1914", "1915", "1916", "1917", "1918", "1919", "1920", "1921", "1922", "1923", "1924" ]
  }
}
